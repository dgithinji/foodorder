import React, {useEffect, useState} from 'react';
import styles from './AvailableMeals.module.css'
import Card from "../UI/Card";
import MealItem from "./MealItem/MealItem";

const AvailableMeals = (props) => {

    const [meals, setMeals] = useState([])
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState();

    useEffect(() => {
        const fetchMeals = async () => {
            const response = await fetch('https://test-51347-default-rtdb.firebaseio.com/meals.json');
            if (!response.ok) {
                throw new Error('something went wrong');
            }
            const responseData = await response.json();

            const loadedMeals = [];

            for (const key in responseData) {
                loadedMeals.push({
                    id: key,
                    name: responseData[key].name,
                    description: responseData[key].description,
                    price: responseData[key].price,
                })
            }

            setMeals(loadedMeals);
            setIsLoading(false)
        }

        fetchMeals().catch(error => {
            setIsLoading(false);
            setError(error.message)
        });
    }, [])


    if (isLoading) {
        return (
            <section className={styles.MealsLoading}>
                <p>Loading...</p>
            </section>
        )
    }

    if (error) {
        return (
            <section className={styles.mealError}>
                <p>{error}</p>
            </section>
        )
    }

    const mealsList = meals.map(meal => (
        <MealItem key={meal.id} id={meal.id} name={meal.name} description={meal.description}
                  price={meal.price}/>
    ));
    return (
        <section className={styles.meals}>
            <Card>
                <ul>
                    {mealsList}
                </ul>
            </Card>
        </section>
    );
};

export default AvailableMeals;
